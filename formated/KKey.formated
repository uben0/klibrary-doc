#REF:#include "KKey.hpp" ➔ namespace KKey
#TITLE:KKey

#TEXT
	A <strong>namespace</strong> that contains <strong>enum</strong> of keyboard keys.
#CODE
	namespace {
		enum Id {...};
	}
#

#SECTION:Member
#ELEMENT
	#CODE
		enum Id {
			Digit0, Digit1, Digit2, Digit3, Digit4, Digit5, Digit6, Digit7, Digit8, Digit9,
			A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
			LShift, LCtrl, LAlt, LBracket, LMeta,
			RShift, RCtrl, RAlt, RBracket, RMeta,
			Left, Right, Up, Down,
			Space, Backspace, Return, Escape, Tab,
			F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
			None
		}
	#TEXT
		List of keyboard key (<code>Meta</code> is the OS dependent key, like Windows or CMD with Mac).
	#
#

#SECTION:Operator
#ELEMENT
	#CODE
		std::ostream& operator<<(std::ostream& ostream, KKey::Id key);
		std::istream& operator>>(std::istream& istream, KKey::Id key);
	#TEXT
		Provide conversion to <code>iostream</code>.
	#
#

#SECTION:Example
#ELEMENT
	#CODE
		KWindow window("example", 512, 256);
		KKey::Id quitKey = KKey::Escape;

		while (window.event[quitKey] == false) {
			KTimer::wait_for(200);
			window.event.update();
		}
	#
#
