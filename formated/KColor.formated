#REF:#include "KColor.hpp" ➔ struct KColor
#TITLE:KColor

#TEXT
	A <strong>struct</strong> that represents a color with R, G, B and A canals.
#CODE
	struct KColor {
		unsigned char r, g, b, a;
	}
#

#SECTION:Constructor
#ELEMENT
	#CODE
		KColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);
	#TEXT
		Set all canals of the color.
	#
#

#SECTION:Member
#ELEMENT
	#CODE
		unsigned char r, g, b, a;
	#TEXT
		Four bytes for each color canals (red, green, blue and alpha).
	#
#

#SECTION:Predefined
#ELEMENT
	#CODE
		KColor Black, White, Transparent;
		KColor Red, Green, Blue;
		KColor Cyan, Magenta, Yellow;
	#TEXT
		Predefined colors.
	#
#

#SECTION:Operator
#ELEMENT
	#CODE
		std::ostream& operator<<(std::ostream& ostream, KColor color);
		std::istream& operator>>(std::istream& istream, KColor color);
	#TEXT
		Provide conversion to <code>iostream</code>.
	#
#

#SECTION:Example
#ELEMENT
	#CODE
		KSurface surface(680, 420, KColor(40, 40, 40));

		KColor color(255, 40, 40);

		for (int i = 0; i < 100; i++) {
			surface.set_pixel(i, i, color);
			color.g += 2;
			color.r -= 1;
		}

		surface.save("example.bmp");
	#
#
