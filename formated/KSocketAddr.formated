#REF:#include "KSocket.hpp" ➔ namespace KSocket
#TITLE:KSocket

#TEXT
	A <strong>namespace</strong> that regroups objects to deal with network sockets.
#

#BUTTON:KSocketAddr
#BUTTON:KSocketUdp
#BUTTON:KSocketTcp

#SECTION:
#SPACE
#SPACE

#REF:#include "KSocket.hpp" ➔ class KSocketAddr
#TITLE:KSocketAddr

#TEXT
	A <strong>class</strong> that holds an IP address (IPv4 or IPv6).
#CODE
	class KSocketAddr {};
#

#SECTION:Constructor
#ELEMENT
	#CODE
		Addr(std::string address, unsigned short port = 0);
	#TEXT
		Creates an IP address from a text address like "192.168.0.34" and a port.
	#
#

#SECTION:Method
#ELEMENT
	#CODE
		bool is_set();
		bool is_empty();
		bool is_ipv4();
		bool is_ipv6();
	#TEXT
		Tests whether the address is valid or not.
		Tests whether the address is IPv4 or IPv6.
	#
#ELEMENT
	#CODE
		unsigned short get_port();
		IpVersion get_ip_version();
	#TEXT
		Returns the port or the ip version (IPv4 or IPv6).
	#
#ELEMENT
	#CODE
		std::string to_string(bool port = true);
	#TEXT
		Returns the address as text (<code>port</code> if port should be include or not)
	#
#

#SECTION:Operator
#ELEMENT
	#CODE
		std::ostream& operator<<(std::ostream& ostream, KSocketAddr addr);
		std::istream& operator>>(std::istream& istream, KSocketAddr addr);
	#TEXT
		Provide conversion to <code>iostream</code>.
	#
#ELEMENT
	#CODE
		bool operator==(KSocketAddr left, KSocketAddr right);
		bool operator!=(KSocketAddr left, KSocketAddr right);
	#TEXT
		Test whether the two addresses are the same or not.
	#
#

#SECTION:Example
#ELEMENT
	#CODE
		KSocketAddr address("127.0.0.1", 4321);

		std::cin >> address;

		KSocketUdp socket;
		socket.open(address.get_ip_version());
		socket.write(address, "hello world");
	#
#
