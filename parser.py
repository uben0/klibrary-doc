import os
import sys

i       = None
tab     = None
fIndent = None
output  = None
classList = [
	"KColor",
	"KControl",
	"KEvent",
	"KFont",
	"KKey",
	"KRect",
	"KSetting",
	"KSocketAddr",
	"KSocketUdp",
	"KSocketTcp",
	"KSurface",
	"KText",
	"KTimer",
	"KWindow"
]

def placeLink(file):
	for className in classList:
		file[i] = file[i].replace(className, "<a href=\"" + className + ".html\">" + className + "</a>")

def generator(inputFile, outputFile):
	global i, tab, fIndent, output
	output = open(outputFile, 'w')
	output.write(open("start.txt", 'r').read())
	fileLines = open(inputFile, 'r').readlines()
	i = 0
	tab = "\t\t\t"
	fIndent = 0
	parser(fileLines, len(fileLines))
	output.write(open("end.txt", 'r').read())

def parser(file, n):
	global i, output
	while i < n:
		file[i] = file[i][fIndent:]
		if file[i].startswith("#SPACE"):
			formatedSpace(file)
		elif file[i].startswith("#TITLE:"):
			formatedTitle(file)
		elif file[i].startswith("#BUTTON:"):
			formatedButton(file)
		elif file[i].startswith("#REF:"):
			formatedRef(file)
		elif file[i].startswith("#SECTION:"):
			formatedSection(file)
		elif file[i] == "#CODE\n":
			formatedCode(file)
		elif file[i] == "#TEXT\n":
			formatedText(file)
		elif file[i] == "#ELEMENT\n":
			formatedElement(file)
		elif file[i] == "#HTML\n":
			formatedHtml(file)
		elif file[i] == "#SCRIPT\n":
			formatedScript(file)
		else:
			i += 1

def formatedSpace(file):
	global i, output
	output.write(tab + "<br>\n")
	i += 1

def formatedTitle(file):
	global i, output
	line = file[i][len("#TITLE:"):]
	line = line.replace("&", "&amp")
	line = line.replace("<", "&lt")
	line = line.replace(">", "&gt")
	line = line.replace("\n", "")
	output.write(tab + "<h1>" + line + "</h1>\n")
	i += 1

def formatedButton(file):
	global i, output
	line = file[i][len("#BUTTON:"):]
	line = line.replace("&", "&amp")
	line = line.replace("<", "&lt")
	line = line.replace(">", "&gt")
	line = line.replace("\n", "")
	output.write(tab + "<a class=\"library-button\" href=\"" + line + ".html\">" + line + "</a>\n")
	i += 1

def formatedRef(file):
	global i, output
	line = file[i][len("#REF:"):]
	line = line.replace("&", "&amp")
	line = line.replace("<", "&lt")
	line = line.replace(">", "&gt")
	line = line.replace("\n", "")
	output.write(tab + "<p class=\"ref\">" + line + "</p>\n")
	i += 1

def formatedSection(file):
	global i, output
	line = file[i][len("#SECTION:"):]
	line = line.replace("&", "&amp")
	line = line.replace("<", "&lt")
	line = line.replace(">", "&gt")
	line = line.replace("\n", "")
	output.write(tab + "<h2>" + line + "</h2>\n")
	i += 1

def formatedCode(file):
	global i, output, fIndent
	i += 1
	fIndent += 1
	newLine = False
	output.write(tab + "<pre><code class=\"hljs cpp\">")
	while len(file[i]) == 1 or file[i][fIndent - 1] == "\t":
		if newLine: output.write("&#10")
		newLine = True
		file[i] = file[i][fIndent:]
		file[i] = file[i].replace("\t", "    ")
		file[i] = file[i].replace("&", "&amp")
		file[i] = file[i].replace("<", "&lt")
		file[i] = file[i].replace(">", "&gt")
		file[i] = file[i].replace("\n", "")
		placeLink(file)
		output.write(file[i])
		i += 1
	fIndent -= 1
	output.write("</code></pre>\n")

def formatedText(file):
	global i, output, fIndent
	i += 1
	fIndent += 1
	output.write(tab + "<p>")
	newSpace = False
	while len(file[i]) == 1 or file[i][fIndent - 1] == "\t":
		if newSpace: output.write("<br>")
		newSpace = file[i].find("\\\n") < 0
		if not newSpace: file[i] = file[i].replace("\\\n", " ")
		file[i] = file[i][fIndent:]
		file[i] = file[i].replace("\n", "")
		placeLink(file)
		output.write(file[i])
		i += 1
	fIndent -= 1
	output.write("</p>\n")

def formatedElement(file):
	global i, output, tab, fIndent
	i += 1
	fIndent += 1
	output.write(tab + "<div class=\"ebox\">\n")
	tab = tab + "\t"
	n = i
	while file[n][fIndent - 1] == "\t" or len(file[n]) == 1:
		n += 1
	parser(file, n)
	tab = tab[:-1]
	fIndent -= 1
	i = n
	output.write(tab + "</div>\n")

def formatedHtml(file):
	global i, output, fIndent
	i += 1
	fIndent += 1
	while len(file[i]) == 1 or file[i][fIndent - 1] == "\t":
		if (len(file[i]) == 1):
			output.write("\n")
		else:
			file[i] = file[i][fIndent:]
			output.write(file[i])
		i += 1
	fIndent -= 1

def formatedScript(file):
	global i, output, fIndent, tab
	i += 1
	fIndent += 1
	output.write(tab + "<script type=\"text/javascript\">\n")
	tab += "\t"
	while len(file[i]) == 1 or file[i][fIndent - 1] == "\t":
		if (len(file[i]) == 1):
			output.write("\n")
		else:
			file[i] = file[i][fIndent:]
			output.write(tab + file[i])
		i += 1
	fIndent -= 1
	tab = tab[:-1]
	output.write(tab + "</script>\n")

if len(sys.argv) == 3:
	generator(sys.argv[1], sys.argv[2])
