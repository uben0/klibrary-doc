MODULE   = $(shell ls formated)
FRAGMENT = start.txt end.txt parser.py
HTML     = $(addprefix generated/, $(MODULE:.formated=.html))
FORMATED = $(addprefix formated/, $(MODULE))
INCLUDE  = $(filter-out generated/include/klibrary-documentation.zip, $(wildcard generated/include/*))
OFFLINE  = generated/include/klibrary-documentation.zip

default: $(HTML) $(OFFLINE)

generated/%.html: formated/%.formated $(FRAGMENT)
	python3 parser.py $< $@

$(OFFLINE): $(HTML) $(INCLUDE)
	cd generated; zip -r include/klibrary-documentation.zip $(INCLUDE:generated/%=%) $(HTML:generated/%=%)

clean:
	rm -f $(HTML) $(OFFLINE)
