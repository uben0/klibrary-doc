# KLibrary documentation

KLibrary documentation generator.

Execute the `Makefile` and it will generate a static website. An instance of this documentation is hosted [here](https://klibrary.uben.ovh).
